# Analysis of Energy Market Integration Colombia and Ecuador

## Overview

The quest for efficient energy management has led to the need for greater integration in electricity markets, particularly in transnational regions like Latin America. This project aims to evaluate this integration in the context of the electricity markets of Colombia and Ecuador. The goal is to conduct a systematic assessment of market integration through a comprehensive literature review, identification of appropriate evaluation metrics, and analysis of historical and current data from International Electricity Transactions (TIE in Spanish). Despite being limited by the availability of public information and focusing exclusively on the electricity markets of Colombia and Ecuador, this work is expected to significantly contribute to the understanding of electricity market integration in the region and offer valuable recommendations for future improvements.

This repository is part of the Master's Thesis in Engineering - Analytics at the National University of Colombia, Medellin.

## Repository Structure

- `Congestion_Rents/`
- `Data_Analysis/`
- `Money/`
- `Prices/`
- `Quantities/`
- `.gitignore`

### Notebooks

- **EDA_Export_and_Import_por_Enlace.ipynb**: Contains the analysis of the energy export and import quantities transacted between Colombia and Ecuador.
- **EDA_PONE_PLiqExport_PImport_por_Enlace.ipynb**: Analysis of the prices used in International Electricity Transactions (TIE), which determine the direction of energy flow or transaction direction.
- **ETL_Congestion_Rents.ipynb**: ETL process for extracting the congestion rents between the electricity market of Colombia and Ecuador.
- **ETL_Money_por_Enlace.ipynb**: ETL process for extracting financial data related to energy transactions.
- **ETL_Prices_por_Enlace.ipynb**: ETL process for extracting price data used in energy transactions.
- **ETL_Quantities_por_Enlace.ipynb**: ETL process for extracting quantity data of energy transacted.

## Data Source

All the data is published by XM S.A. E.S.P. in [Sinergox](https://sinergox.xm.com.co/).

## Purpose

The purpose of this repository is to provide the necessary tools and analyses for understanding and evaluating the integration of electricity markets between Colombia and Ecuador. By analyzing historical and current data, we aim to offer insights and recommendations to improve market integration.

## Contributions

This work is intended to contribute significantly to the comprehension of electricity market integration in the Latin American region and to provide valuable recommendations for enhancing this integration in the future.